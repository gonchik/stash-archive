define('bitbucket/internal/feature/repository/repository-download', [
    'jquery',
    'bitbucket/util/events',
    'bitbucket/util/navbuilder',
    'bitbucket/internal/model/page-state',
    'exports'
], function(
    $,
    events,
    navBuilder,
    pageState,
    exports
) {
    'use strict';

    exports.onReady = function (buttonSelector) {
        var $button = $(buttonSelector);

        /**
         * Update the "Download" button's URL to target the specified ref.
         *
         * @param revisionRef the ref that the archive button should target. Falsey means no ref to be set, which will
         * cause the server to default to HEAD of the default branch.
         */
        var updateDownloadRef = function(revisionRef) {
            var builder = navBuilder.rest('archive').currentRepo().addPathComponents("archive");
            if (revisionRef && !revisionRef.isDefault()) {
                builder = builder.withParams({
                    at: revisionRef.getId()
                });
            }

            $button.attr('href', builder.withParams({format: 'zip'}).build());
        };

        // On page load, grab the current ref from the current page's state
        updateDownloadRef(pageState.getRevisionRef());

        // Also, bind to the branch selector's change event to grab the newly selected ref
        events.on('bitbucket.internal.feature.repository.revisionReferenceSelector.revisionRefChanged',
            function(revisionRef, context) {
                updateDownloadRef(revisionRef);
            });
    }
});

jQuery(document).ready(function () {
    require('bitbucket/internal/feature/repository/repository-download').onReady('#download-repo-button');
});