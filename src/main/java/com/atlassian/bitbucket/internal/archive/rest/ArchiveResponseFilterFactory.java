package com.atlassian.bitbucket.internal.archive.rest;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.*;

import javax.annotation.Nonnull;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

/**
 * Wraps {@link ArchiveResponse} implementations in a helper shim which attempts to map any {@code RuntimeException}s
 * they throw to {@code Response}s and write them on the response, if the response isn't already committed.
 *
 * @since 2.1
 */
public class ArchiveResponseFilterFactory implements ResourceFilterFactory {

    private final List<ResourceFilter> filters;

    public ArchiveResponseFilterFactory() {
        filters = Collections.singletonList(new ArchiveResponseFilter());
    }

    @Override
    public List<ResourceFilter> create(AbstractMethod restMethod) {
        Method method = restMethod.getMethod();
        if (ArchiveResource.class.equals(method.getDeclaringClass()) && "streamArchive".equals(method.getName())) {
            //Only requests to ArchiveResource.streamArchive could possibly include an ArchiveResponse, so they're
            //the only method calls we want to add the filter to
            return filters;
        }
        return Collections.emptyList();
    }

    private static class ArchiveResponseFilter implements ContainerResponseFilter, ResourceFilter {

        @Override
        public ContainerResponse filter(ContainerRequest request, ContainerResponse response) {
            Object entity = response.getEntity();
            if (entity instanceof ArchiveResponse) {
                response.setEntity(new ExceptionMappingArchiveResponse((ArchiveResponse) entity, response));
            }

            return response;
        }

        @Override
        public ContainerRequestFilter getRequestFilter() {
            return null;
        }

        @Override
        public ContainerResponseFilter getResponseFilter() {
            return this;
        }
    }

    private static class ExceptionMappingArchiveResponse implements ArchiveResponse {

        private final ArchiveResponse delegate;
        private final ContainerResponse response;

        ExceptionMappingArchiveResponse(ArchiveResponse delegate, ContainerResponse response) {
            this.delegate = delegate;
            this.response = response;
        }

        @Override
        public void write(@Nonnull MultivaluedMap<String, Object> httpHeaders, @Nonnull OutputStream outputStream)
                throws IOException, WebApplicationException {
            try {
                delegate.write(httpHeaders, outputStream);
            } catch (IOException | RuntimeException e) {
                if (response.isCommitted() || !response.mapException(e)) {
                    //If the response has been committed (meaning the delegate wrote to the OutputStream), or if the
                    //exception that was thrown could not be mapped, just rethrow it. This will propagate up to the
                    //servlet engine and do...whatever it does.
                    throw e;
                }

                //If the response was _not_ previously committed and the exception could be mapped, then write out
                //the mapped response. This way the caller gets a nice, well-formed error result
                response.write();
            }
        }
    }
}