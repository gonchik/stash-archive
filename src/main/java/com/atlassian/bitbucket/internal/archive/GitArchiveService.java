package com.atlassian.bitbucket.internal.archive;

import com.atlassian.bitbucket.archive.ArchiveFormat;
import com.atlassian.bitbucket.archive.ArchiveRequest;
import com.atlassian.bitbucket.archive.ArchiveService;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.internal.archive.command.ArchiveCommandExitHandler;
import com.atlassian.bitbucket.internal.archive.command.ArchiveOutputHandler;
import com.atlassian.bitbucket.io.TypeAwareOutputSupplier;
import com.atlassian.bitbucket.repository.InvalidRefNameException;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.throttle.ThrottleService;
import com.atlassian.bitbucket.throttle.Ticket;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.io.OutputStream;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of the {@link ArchiveService} which uses the {@link GitCommandBuilderFactory} to fork
 * {@code git archive} processes.
 *
 * @since 2.1
 */
public class GitArchiveService implements ArchiveService {

    private static final String PROP_PREFIX = "plugin.bitbucket-archive.";

    private static final String PROP_TIMEOUT_PREFIX = PROP_PREFIX + "timeout.";
    private static final String PROP_TIMEOUT_EXECUTION = PROP_TIMEOUT_PREFIX + "execution";
    private static final String PROP_TIMEOUT_IDLE = PROP_TIMEOUT_PREFIX + "idle";

    private final GitCommandBuilderFactory builderFactory;
    private final I18nService i18nService;
    private final ThrottleService throttleService;

    private final long executionTimeout;
    private final long idleTimeout;

    public GitArchiveService(GitCommandBuilderFactory builderFactory, I18nService i18nService,
                             ApplicationPropertiesService propertiesService, ThrottleService throttleService) {
        this.builderFactory = builderFactory;
        this.i18nService = i18nService;
        this.throttleService = throttleService;

        // Ensure the timeout is at least 5 minutes, since producing archives for anything but a trivial repository
        // may take a similar amount of time to a hosting command
        executionTimeout = Math.max(300L, propertiesService.getPluginProperty(PROP_TIMEOUT_EXECUTION, 1800L));
        idleTimeout = Math.max(60L, propertiesService.getPluginProperty(PROP_TIMEOUT_IDLE, 300L));
    }

    public void stream(@Nonnull Repository repository, ArchiveFormat format, @Nonnull String ref,
                       @Nonnull OutputStream outputStream) {
        ArchiveRequest.Builder builder = new ArchiveRequest.Builder(repository, ref);
        if (format != null) {
            builder.format(format);
        }

        stream(builder.build(), contentType -> outputStream);
    }

    public void stream(@Nonnull ArchiveRequest request, @Nonnull TypeAwareOutputSupplier outputSupplier) {
        String commitId = requireNonNull(request, "request").getCommitId();
        if (StringUtils.startsWith(StringUtils.stripStart(commitId, null), "-")) {
            throw new InvalidRefNameException(
                    i18nService.createKeyedMessage("bitbucket.archive.invalidref", commitId),
                    commitId);
        }

        // Since git archive operations can be reasonably expensive, this resource first acquires an "scm-hosting"
        // ticket. This limits the number of concurrent archive operations that can occur simultaneously and conserves
        // server resources - see ThrottleService for more details. Note that repository hosting resources
        // also use the "scm-hosting" name, so archive operations will be lumped in the same bucket as a push or clone.
        try (Ticket ignored = throttleService.acquireTicket("scm-hosting")){
            // Create & call a new git-archive command in the target repository with the requested parameters
            GitScmCommandBuilder builder = builderFactory.builder(request.getRepository())
                    .command("archive")
                    .exitHandler(new ArchiveCommandExitHandler(i18nService, request.getRepository(), commitId))
                    .argument("--format=" + request.getFormat().getDefaultExtension());
            request.getPrefix().ifPresent(prefix -> builder.argument("--prefix=" + prefix));

            // Put the -- in before the commit ID to ensure it can't be treated as an option (e.g. --remote)
            builder.argument("--")
                    .argument(commitId);
            request.getPaths().forEach(builder::argument);

            GitCommand<Void> command = builder.build(new ArchiveOutputHandler(request.getFormat(), outputSupplier));
            command.setExecutionTimeout(executionTimeout);
            command.setIdleTimeout(idleTimeout);
            command.call();
        }
    }
}
