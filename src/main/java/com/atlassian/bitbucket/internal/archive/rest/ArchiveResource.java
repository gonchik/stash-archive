package com.atlassian.bitbucket.internal.archive.rest;

import com.atlassian.bitbucket.archive.ArchiveService;
import com.atlassian.bitbucket.commit.SimpleMinimalCommit;
import com.atlassian.bitbucket.archive.ArchiveFormat;
import com.atlassian.bitbucket.archive.ArchiveRequest;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.Ref;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.ResolveRefRequest;
import com.atlassian.bitbucket.rest.RestErrors;
import com.atlassian.bitbucket.rest.util.ResourcePatterns;
import com.atlassian.bitbucket.rest.util.ResponseFactory;
import com.atlassian.bitbucket.validation.ArgumentValidationException;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.atlassian.bitbucket.util.MoreCollectors.toImmutableList;

/**
 * @since 2.1
 */
@AnonymousAllowed
@Path(ResourcePatterns.REPOSITORY_URI + "/archive")
@Produces({MediaType.APPLICATION_OCTET_STREAM, "application/x-tar"})
@Singleton
public class ArchiveResource {

    private final ArchiveService archiveService;
    private final I18nService i18nService;
    private final RefService refService;

    public ArchiveResource(ArchiveService archiveService, I18nService i18nService, RefService refService) {
        this.archiveService = archiveService;
        this.i18nService = i18nService;
        this.refService = refService;
    }

    /**
     * Streams an archive of the repository's contents at the requested commit. If no <code>at=</code> commit is
     * requested, an archive of the default branch is streamed.
     * <p>
     * The <code>filename=</code> query parameter may be used to specify the exact filename to include in the
     * <code>"Content-Disposition"</code> header. If an explicit filename is not provided, one will be automatically
     * generated based on what is being archived. Its format depends on the <code>at=</code> value:
     * <ul>
     *     <li>No <code>at=</code> commit:
     *     <code>&lt;slug&gt;-&lt;default-branch-name&gt;@&lt;commit&gt;.&lt;format&gt;</code>;
     *     e.g. example-master@43c2f8a0fe8.zip</li>
     *     <li><code>at=sha</code>: <code>&lt;slug&gt;-&lt;at&gt;.&lt;format&gt;</code>; e.g.
     *     example-09bcbb00100cfbb5310fb6834a1d5ce6cac253e9.tar.gz</li>
     *     <li><code>at=branchOrTag</code>: <code>&lt;slug&gt;-&lt;branchOrTag&gt;@&lt;commit&gt;.&lt;format&gt;</code>;
     *     e.g. example-feature@bbb225f16e1.tar
     *     <ul>
     *         <li>If the branch or tag is qualified (e.g. <code>refs/heads/master</code>, the short name
     *         (<code>master</code>) will be included in the filename</li>
     *         <li>If the branch or tag's <i>short name</i> includes slashes (e.g. <code>release/4.6</code>),
     *         they will be converted to hyphens in the filename (<code>release-4.5</code>)</li>
     *     </ul>
     *     </li>
     * </ul>
     * <p>
     * Archives may be requested in the following formats by adding the <code>format=</code> query parameter:
     * <ul>
     *     <li><code>zip</code>: A zip file using standard compression (Default)</li>
     *     <li><code>tar</code>: An uncompressed tarball</li>
     *     <li><code>tar.gz</code> or <code>tgz</code>: A GZip-compressed tarball</li>
     * </ul>
     * The contents of the archive may be filtered by using the <code>path=</code> query parameter to specify paths to
     * include. <code>path=</code> may be specified multiple times to include multiple paths.
     * <p>
     * The <code>prefix=</code> query parameter may be used to define a directory (or multiple directories) where
     * the archive's contents should be placed. If the prefix does not end with <code>/</code>, one will be added
     * automatically. The prefix is <i>always</i> treated as a directory; it is not possible to use it to prepend
     * characters to the entries in the archive.
     * <p>
     * Archives of public repositories may be streamed by any authenticated or anonymous user. Streaming archives for
     * non-public repositories requires an <i>authenticated user</i> with at least <b>REPO_READ</b> permission.
     *
     * @param commitId the commit to stream an archive of; if not supplied, an archive of the default branch is streamed
     * @param filename a filename to include the "Content-Disposition" header
     * @param format   the format to stream the archive in; must be one of: zip, tar, tar.gz or tgz
     * @param paths    paths to include in the streamed archive; may be repeated to include multiple paths
     * @param prefix   a prefix to apply to all entries in the streamed archive; if the supplied prefix does not end
     *                 with a trailing <code>/</code>, one will be added automatically
     *
     * @response.representation.200.mediaType application/octet-stream; application/x-tar
     * @response.representation.200.qname archive
     * @response.representation.200.doc An archive or the requested commit, in zip, tar or gzipped-tar format.
     *
     * @response.representation.400.mediaType application/json
     * @response.representation.400.qname errors
     * @response.representation.400.example {@link RestErrors#EXAMPLE}
     * @response.representation.400.doc The requested <code>format</code> is not supported.
     *
     * @response.representation.401.mediaType application/json
     * @response.representation.401.qname errors
     * @response.representation.401.example {@link RestErrors#EXAMPLE}
     * @response.representation.401.doc The currently authenticated user has insufficient permissions
     *                                  to view the repository.
     *
     * @response.representation.404.mediaType application/json
     * @response.representation.404.qname errors
     * @response.representation.404.example {@link RestErrors#EXAMPLE}
     * @response.representation.404.doc The repository does not exist or does not contain the <code>at</code> commit.
     */
    @GET
    public Response streamArchive(@Context Repository repository, @QueryParam("at") String commitId,
                                  @QueryParam("filename") String filename, @QueryParam("format") String format,
                                  @QueryParam("path") List<String> paths, @QueryParam("prefix") String prefix) {
        Ref ref = null;
        if (commitId == null) {
            ref = refService.getDefaultBranch(repository);
            commitId = ref.getLatestCommit();
        }

        ArchiveRequest request = new ArchiveRequest.Builder(repository, commitId)
                .format(parseFormat(format))
                .paths(paths)
                .prefix(prefix)
                .build();

        if (filename == null) {
            if (ref == null) {
                //If the caller has not specified a filename, try to resolve the commitId to a ref. If commitId
                //is actually a ref ID (like master), or is a commit which references the tip of a branch or tag,
                //the ref's display ID will be used to help build a default filename
                ResolveRefRequest resolveRefRequest = new ResolveRefRequest.Builder(repository)
                        .refId(commitId)
                        .build();
                ref = refService.resolveRef(resolveRefRequest);
            }

            filename = buildFilename(request, ref);
        }
        String contentDisposition = "attachment; filename=\"" + filename + "\"";

        //Using ArchiveResponse here relies on filtering applies by ArchiveResponseFilterFactory. Without that,
        //the entity will not be marshaled correctly
        return ResponseFactory.ok((ArchiveResponse) (headers, output) ->
                archiveService.stream(request, contentType -> {
                    headers.putSingle("Content-Disposition", contentDisposition);
                    headers.putSingle("Content-Type", contentType);

                    return output;
                })
        ).build();
    }

    private static String buildFilename(ArchiveRequest request, Ref ref) {
        StringBuilder builder = new StringBuilder(request.getRepository().getSlug())
                .append("-");
        if (ref == null) {
            builder.append(replaceSlashes(request.getCommitId()));
        } else {
            builder.append(replaceSlashes(ref.getDisplayId()))
                    .append('@')
                    .append(ref.getLatestCommit().substring(0, SimpleMinimalCommit.DEFAULT_DISPLAY_ID_LENGTH));
        }

        return builder.append(".")
                .append(request.getFormat().getDefaultExtension())
                .toString();
    }

    private static String replaceSlashes(String value) {
        return value.replace('/', '-');
    }

    private ArchiveFormat parseFormat(String value) {
        value = StringUtils.trimToNull(value);
        if (value == null) {
            return ArchiveFormat.ZIP;
        }

        try {
            //Accept input like "tar.gz", "tar-gz" or "TAR_GZ" and return ArchiveFormat.TAR_GZ
            return ArchiveFormat.fromExtension(value.replaceAll("[_-]", "."));
        } catch (IllegalArgumentException e) {
            List<String> supportedFormats = Stream.of(ArchiveFormat.values())
                    .map(ArchiveFormat::getExtensions)
                    .flatMap(Set::stream)
                    .collect(toImmutableList());

            throw new ArgumentValidationException(i18nService.createKeyedMessage(
                    "bitbucket.rest.archive.unsupportedformat", value, supportedFormats));
        }
    }
}